import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.time.LocalDateTime;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultCaret;

/**
 * A simple text chat program
 * 
 * Messages are broken up into packetSize blocks and sent to the destination IP
 * via TCP.
 * 
 * As I understand it, there are two TCP connections but I am not sure which end
 * initiates the connections. The code in initNetwork below initiates the tx and
 * accepts the rx - should be easy to modify if required.
 * 
 * Messages are sent in 64 byte blocks (padded to 64 bytes if shorter). The UI 
 * adds carriage returns.
 * 
 * @author Mark
 *
 */
public class Chat {
	private final JTextArea history = new JTextArea();
	private final JTextField messageField = new JTextField();
	private boolean running = true;
	private Thread rxThread = null;
	private final int rxPort;
	private final int txPort;
	private final String txIp;
	private final int packetSize;
	private boolean connected = false;
	private DataOutputStream outStream = null;
	private final JFrame frame = new JFrame("Chat");
	private Socket clientSocket = null;

	public Chat(int rxPort, int txPort, String txIp, int packetSize) {
		this.rxPort = rxPort;
		this.txPort = txPort;
		this.txIp = txIp;
		this.packetSize = packetSize;
		initNetwork();
		initGui();
	}

	private void initNetwork() {
		// start a thread for the server
		// TODO: Should this be doing an outbound connection to RH as well?
		rxThread = new Thread() {
			public void run() {
				ServerSocket serverSocket = null;
				try {
					serverSocket = new ServerSocket(rxPort);
					serverSocket.setSoTimeout(1000);
					System.out.println("Now accepting connections...");

					// We will only have 1 connection active at a time
					while (running) {

						try {
							final Socket rxSocket = serverSocket.accept();

							new Thread() {
								public void run() {
									try {
										System.out.println("Connection from " + rxSocket.getInetAddress());
										BufferedReader reader = new BufferedReader(
												new InputStreamReader(rxSocket.getInputStream()));

										char[] buffer = new char[packetSize];
										int bytesRead = 0;
										while ((bytesRead = reader.read(buffer)) > 0) {
											System.out.println("Read " + bytesRead + " bytes");
											appendMessage(new String(buffer), false);
										}
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							}.start();
						} catch (SocketTimeoutException e) {
						}
					}
				} catch (IOException e) {
					System.out.println("Error on incoming connection.");
					e.printStackTrace();
					System.exit(1);
				} finally {
					if (serverSocket != null) {
						try {
							serverSocket.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		};

		rxThread.start();

		// Make the outgoing connection..
		while (!connected) {

			try {
				System.out.println("Connecting...");
				clientSocket = new Socket(txIp, txPort);
				outStream = new DataOutputStream(clientSocket.getOutputStream());
				System.out.println("Connected");
				connected = true;
			} catch (Exception e) {
				System.out.println("Connection attempt failed, waiting 3 seconds (" + e.getMessage() + ")");
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
				}
			}
		}
	}

	private void appendMessage(final String text, final boolean wasSent) {
		// Make sure we are on the swing thread...
		if (SwingUtilities.isEventDispatchThread() == false) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					appendMessage(text, wasSent);
				}
			});
			return;
		}

		LocalDateTime now = LocalDateTime.now();
		String message = now.getHour() + ":" + now.getMinute() + ":" + now.getSecond();
		if (wasSent)
			message = message + " >>> " + text;
		else
			message = message + " <<< " + text;
		history.append(message + "\n");
	}

	private void sendMessage(String message) {
		ByteArrayInputStream msgBuffer = new ByteArrayInputStream(message.getBytes());
		byte[] byteBuffer = new byte[packetSize];

		for (int i = 0; i < packetSize; i++)
			byteBuffer[i] = ' ';
		try {
			int byteCount;
			while ((byteCount = msgBuffer.read(byteBuffer)) > 0) {
				System.out.println("Sending " + byteCount + " bytes...");
				outStream.write(byteBuffer);
				outStream.flush();
				for (int i = 0; i < packetSize; i++)
					byteBuffer[i] = ' ';

			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	private void handleSendMessage ()
	{
		String message = messageField.getText();
		messageField.setText("");
		sendMessage(message);
		appendMessage(message, true);	
	}

	private void initGui() {
		history.setEditable(false);
		history.setBackground(new Color(32, 48, 32));
		history.setForeground(Color.green);
		history.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
		DefaultCaret caret = (DefaultCaret) history.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(new JScrollPane(history), BorderLayout.CENTER);

		JButton sendButton = new JButton("Send");

		sendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				handleSendMessage();
			}
		});
		
		// Add another listener so we send messages when enter is pressed...
		messageField.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				handleSendMessage();
			}
		});
		messageField.setBackground(new Color(32, 48, 32));
		messageField.setForeground(Color.GREEN);

		JPanel messagePanel = new JPanel();
		messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.X_AXIS));
		messagePanel.add(messageField);
		messagePanel.add(Box.createRigidArea(new Dimension(5, 5)));
		messagePanel.add(sendButton);
		messagePanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		messagePanel.setBackground(Color.black);
		panel.add(messagePanel, BorderLayout.SOUTH);
		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setBackground(Color.black);

		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				shutdown();
			}
		});
		frame.getContentPane().add(panel);
		frame.setSize(640, 480);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		messageField.requestFocus();
	}

	private void shutdown() {
		System.out.println("Shutting down...");
		frame.dispose();
		System.out.println("Killing outgoing connection...");
		if (connected) {
			try {
				outStream.close();
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		System.out.println("Killing server connection...");
		running = false;
		while (rxThread.isAlive()) {
			try {
				rxThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		System.out.println("All done. Cya.");
	}

	public static void main(String[] args) {
		int rxPort = -1, txPort = -1, packetSize = 64;
		String txIp = "";
		try {
			rxPort = Integer.parseInt(args[0]);
			txIp = args[1];
			txPort = Integer.parseInt(args[2]);

			if (args.length == 4)
				packetSize = Integer.parseInt(args[3]);
		} catch (Exception e) {
			System.out.println("Error parsing command line arguments. Usage:");
			System.out.println("   Chat rxPort txIp txPort {packetSize}");
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println("Chat starting...");
		System.out.println("   listening port = " + rxPort);
		System.out.println("   destination = " + txIp + ":" + txPort);
		System.out.println("   packet size = " + packetSize + " bytes");

		new Chat(rxPort, txPort, txIp, packetSize);
	}
}
